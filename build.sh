#!/bin/bash

apt-get update
apt-get -y install build-essential git uuid uuid-dev zlib1g-dev liblzo2-dev liblzo2-dev u-boot-tools openjdk-7-jdk bison g++-multilib git gperf libxml2-utils

if [ -d /root/android ]; then rm -rf /root/android; fi
mkdir /root/android
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > /tmp/repo
chmod +x /tmp/repo
pushd /root/android
/tmp/repo init -u https://android.googlesource.com/platform/manifest -b android-4.4.2_r1
/tmp/repo sync
source < /root/android >./and_patch.sh
c_patch < /root/android >. imx_kk4.4.2_1.0.0-ga
